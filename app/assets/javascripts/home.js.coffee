jQuery ->
  @cancel_modal_button = $("a.modal-cancel")
  @cancel_modal_button.bind "click", $.modal_cancel

  @provider_popup_open_button = $(".provider-popup-open")
  @provider_popup_open_button.bind "click", $.provider_popup_open

$.modal_cancel = ()->
  modal_id = "#" + $(@).attr("data-trigger")
  $(modal_id).trigger("reveal:close")

$.provider_popup_open = ()->
  offsetX   = window.screenLeft || window.screenX
  offsetY   = window.screenTop  || window.screenY
  width     = 700
  height    = 460
  top       = offsetY + (($(document).height() - height) / 6)
  left      = offsetX + (($(document).width() - width) / 2)
  features  = "width=#{width},height=#{height},left=#{left},top=#{top}"

  window.open($(this).data("url"), "AchievedBy.com", features).focus()

$.close_opener = (reload_parent = false)->
  if window.opener
    if reload_parent
      window.opener.location.reload()
    window.close()