class Video < ActiveRecord::Base
  belongs_to :user
  belongs_to :achievement

  attr_accessible :url

  validates :url, :presence => true

  before_save :youtube_embed

  protected

  def youtube_embed
    if self.url[/youtu\.be\/([^\?]*)/]
      self.url = $1
    else
      self.url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      self.url = $5
    end
  end
end
