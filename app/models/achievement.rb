class Achievement < ActiveRecord::Base
  belongs_to :user
  has_one    :video
  has_one    :picture

  attr_accessible :title, :content, :video_attributes
  accepts_nested_attributes_for :video, :reject_if => lambda { |v| v[:url].blank? }, :allow_destroy => true

  validates :title, :presence => true
end
