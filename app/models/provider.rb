class Provider < ActiveRecord::Base
  belongs_to :user, :touch => true

  attr_accessible :name, :uid, :access_token

  scope :by_name, lambda{ |name| where(:name => name)}
end
