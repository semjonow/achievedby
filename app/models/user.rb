class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, :use => :slugged

  has_many :achievements
  has_many :providers

  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :validatable, :omniauthable

  attr_accessible :email, :username, :password, :password_confirmation
  attr_accessor   :login

  validates :username, :presence => true, :uniqueness => true, :length => { :in => 2..50 }

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(['lower(username) = :value OR lower(email) = :value', { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource = nil)
    provider_name, uid, email, username, access_token = auth.provider, auth.uid, auth.info.email, auth.info.nickname, auth.credentials.token
    User.find_or_create_and_confirm_by(
        provider_name,
        uid,
        access_token,
        email,
        username,
        signed_in_resource)
  end

  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    provider_name, uid, username, access_token = auth.provider, auth.uid, auth.info.nickname, auth.credentials.token
    User.find_or_create_and_confirm_by(
        provider_name,
        uid,
        access_token,
        nil,
        username,
        signed_in_resource)
  end

  protected

  def self.find_or_create_and_confirm_by(provider_name, uid, access_token, email = nil, username = nil, signed_in_resource = nil)
    provider = Provider.where(:name => provider_name, :uid => uid).first

    unless provider
      user = email.nil? ? nil : User.find_by_email(email)

      if user.nil?
        if signed_in_resource.nil?
          user = User.new({
            :username => User.generate_random_username(username),
            :email    => email.nil? ? User.generate_random_email : email,
            :password => Devise.friendly_token[4,20]
          })
          user.skip_confirmation!
          user.save!
        else
          user = signed_in_resource
        end
      end

      similar_provider = user.providers.where(:name => provider_name).first
      if similar_provider.nil?
        user.providers.create!(
            :name         => provider_name,
            :uid          => uid,
            :access_token => access_token
        )
      else
        similar_provider.update_attributes!({
          :uid          => uid,
          :access_token => access_token
        })
      end
    else
      user = provider.user
    end
    user
  end

  def self.generate_random_email
    random_email = "user.#{Random.rand(99999999)}@achievedby.com"
    return random_email unless User.exists?(email: random_email)
    User.generate_random_email
  end

  def self.generate_random_username(username = nil)
    random_username = username || "user.#{Random.rand(99999999)}"
    return random_username unless User.exists?(username: random_username)
    User.generate_random_username(nil)
  end
end
