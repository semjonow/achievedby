module AchievementsHelper
  def achievement_name
    :achievement
  end

  def achievement
    @achievement ||= current_user.achievements.build
    @video = @achievement.build_video if @achievement.video.nil?
    @achievement
  end

  def video
    @video || achievement
    @video
  end

  def display_video_for(achievement)
    unless achievement.video.nil?
      video = YOUTUBE_CLIENT.video_by(achievement.video.url)
      raw video.embed_html5({:class => 'video-player', :width => '400', :height => '250', :frameborder => '1'})
      #content_tag("iframe", "", :type => "text/html", :allowscriptaccess => "always", :width => 400, :height => 250, :src => "http://www.youtube.com/embed/#{achievement.video.url}?enablejsapi=1")
    end
  end
end
