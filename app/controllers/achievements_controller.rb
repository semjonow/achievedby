class AchievementsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show]
  layout false, :except => [:show]

  def create
    @achievement = current_user.achievements.build(params[:achievement])
    unless @achievement.video.nil?
      @achievement.video.user = current_user
    end
    @achievement.save

    respond_to do |format|
      format.js{ render :locals => { :achievement => @achievement } }
    end
  end
end
