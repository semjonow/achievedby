class Users::PasswordsController < Devise::PasswordsController
  skip_before_filter :require_no_authentication, only: [:new, :edit, :update]
  layout false, :except => [:edit, :update]

  def new
    if request.xhr?
      if user_signed_in?
        current_user.send_reset_password_instructions
        flash[:notice] = I18n.t 'devise.passwords.send_instructions'
        redirect_to :back
      else
        super
      end
    else
      redirect_to root_path
    end
  end

  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    if successfully_sent?(resource)
      respond_with({})
    else
      respond_with(resource)
    end
  end

  def edit
    super
  end

  def update
    super
  end
end
