class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    authorize
  end

  def twitter
    @user = User.find_for_twitter_oauth(request.env["omniauth.auth"], current_user)
    authorize
  end

  def destroy
    provider = current_user.providers.find_by_name(params[:name])
    unless provider.nil?
      provider.destroy
      flash[:notice] = "#{params[:name].capitalize} connection successfully deleted."
    end
    redirect_to :back
  end

  protected

  def authorize
    if @user.persisted?
      sign_in @user, :event => :authentication unless user_signed_in?
    else
      session["devise.#{provider}_data"] = request.env["omniauth.auth"]
    end
    respond_to do |format|
      format.html{ render :layout => "application" }
    end
  end
end
