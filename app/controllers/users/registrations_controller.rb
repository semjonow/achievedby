class Users::RegistrationsController < Devise::RegistrationsController
  layout false, :except => [:edit, :update]

  def new
    redirect_to root_path
  end

  def create
    build_resource

    if resource.save
      if resource.active_for_authentication?
        sign_in(resource_name, resource)
        respond_with resource
      else
        expire_session_data_after_sign_in!
        respond_with resource
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def edit
    super
  end

  def update
    super
  end

  def destroy
    resource.destroy
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  end

  private

  def after_update_path_for(resource)
    edit_user_registration_path
  end
end
