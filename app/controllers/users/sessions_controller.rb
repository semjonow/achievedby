class Users::SessionsController < Devise::SessionsController
  layout false

  def new
    if request.xhr?
      respond_to do |format|
        format.html{ render :layout => "application" }
        format.js
      end
    else
      redirect_to root_path
    end
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    respond_with resource
  end

  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    respond_to do |format|
      format.any(*navigational_formats) { redirect_to redirect_path }
      format.all do
        head :no_content
      end
    end
  end
end
