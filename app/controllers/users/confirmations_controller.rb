class Users::ConfirmationsController < Devise::ConfirmationsController
  def new
    redirect_to root_path
  end

  def create
    super
  end

  def show
    super
  end
end
