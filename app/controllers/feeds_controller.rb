class FeedsController < ApplicationController
  def show
    @user = User.find(params[:user_id])
    unless @user.nil?
      @achievements = @user.achievements
    end
  end
end
