Achievedby::Application.routes.draw do
  root :to => "home#index"

  devise_for :users,
   :path => "",
   :path_names  => {
       :sign_in      => "signin",
       :sign_out     => "signout",
       :sign_up      => "",
       :registration => "signup"
   },
   :controllers => {
       :sessions           => "users/sessions",
       :registrations      => "users/registrations",
       :confirmations      => "users/confirmations",
       :passwords          => "users/passwords",
       :omniauth_callbacks => 'users/omniauth_callbacks'
   }

  devise_scope :user do
    scope "auth" do
      match ":name/destroy" => "users/omniauth_callbacks#destroy", via: :delete, as: :omniauth_callback
    end
    scope ":user_id" do
      resource :feed
    end
  end

  resources :achievements
end
