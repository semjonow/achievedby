class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.integer :user_id,        :null => false
      t.integer :achievement_id, :null => false
      t.string  :url,            :null => false, :default => ""
      t.timestamps
    end

    add_foreign_key(:videos, :users, :dependent => :delete)
    add_foreign_key(:videos, :achievements, :dependent => :delete)
  end
end
