class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.integer :user_id,        :null => false
      t.integer :achievement_id, :null => false

      t.string   :data_file_name
      t.string   :data_content_type
      t.string   :data_file_size
      t.datetime :data_updated_at

      t.timestamps
    end

    add_foreign_key(:pictures, :users, :dependent => :delete)
    add_foreign_key(:pictures, :achievements, :dependent => :delete)
  end
end
