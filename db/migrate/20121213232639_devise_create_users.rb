class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      t.string :email,              :null => false, :default => ""
      t.string :username,           :null => false, :default => ""
      t.string :password_salt,      :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""
      t.string :slug,               :null => false, :default => ""

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email

      t.timestamps
    end

    add_index :users, :email,                :unique => true
    add_index :users, :username,             :unique => true
    add_index :users, :reset_password_token, :unique => true
    add_index :users, :confirmation_token,   :unique => true
    add_index :users, :slug,                 :unique => true
  end
end
