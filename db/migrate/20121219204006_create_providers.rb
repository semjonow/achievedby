class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.integer :user_id,      :null => false
      t.string  :name,         :null => false, :default => ""
      t.string  :uid,          :null => false, :default => ""
      t.string  :access_token, :null => false, :default => ""

      t.timestamps
    end

    add_index(:providers, [:name, :uid], :unique => true)
    add_index(:providers, [:name, :access_token], :unique => true)
    add_foreign_key(:providers, :users, :dependent => :delete)
  end
end
