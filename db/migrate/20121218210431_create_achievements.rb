class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.integer :user_id, :null => false
      t.string  :title,   :null => false, :default => ""
      t.text    :content, :null => false, :default => ""

      t.timestamps
    end

    add_foreign_key(:achievements, :users, :dependent => :delete)
  end
end
